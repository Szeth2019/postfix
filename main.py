from postfixEval import postfixEval
from infixToPostfix import infixToPostfix

def main():
    
    pf1 = "( A + B * C + D )"
    pf2 = "( A + B ) * ( C + D )"
    pf3 = "A * B + C * D"
    pf4 = "A + B + C + D"
    pf5 = "( ( A + B ) * C - D ) * E"
    pf6 = "A * ( B + C )"

    print(infixToPostfix(pf1))
    print(infixToPostfix(pf2))
    print(infixToPostfix(pf3))
    print(infixToPostfix(pf4))
    print(infixToPostfix(pf5))
    print(infixToPostfix(pf6))

    pfexpr1 = "4 5 6 * +"
    pfexpr2 = "7 8 + 3 2 + /"
    pfexpr3 = "2 3 * 5 4 * + 9 -"

    print(postfixEval(pfexpr1))
    print(postfixEval(pfexpr2))
    print(postfixEval(pfexpr3))

if __name__ == '__main__':
    main()
   
   
   
   

   
