class Stack:

	def __init__(self):
		self.items = []

	def isEmpty(self):
		return self.items == []

	def push(self, item):
		self.items.append(item)

	def pop(self):
		return self.items.pop()

	def peek(self):
		return self.items[len(self.items)-1]

	def size(self):
		return len(self.items)

	def printStack(self):

		stackString = ""

		while not self.isEmpty():
			stackString = stackString + str(self.pop())

		print(stackString)

def doMath(op1, op2, op):

	if op == "*":
		return int(op1) * int(op2)
	elif op == "/":
		return int(op1) / int(op2)
	elif op == "+":
		return int(op1) + int(op2)
	elif op == "-":
		return int(op1) - int(op2)
	else:
		print("Incorrect input.")
		return None	
