from mystack import Stack

def infixToPostfix(expr):

# precedence of operators implemented in a dictionary
    prec = {}
    prec["*"] = 3
    prec["/"] = 3
    prec["+"] = 2
    prec["-"] = 2
    prec["("] = 1
   
    # create stack to hold operatorsa
    stack = Stack()
   
    # solution in list form
    postfixexpr = []

    infixList = expr.split()
   
    for item in infixList:
        # if item is an operand, add it to the list postfixexpr
        if (item >= 'A' and item <= 'Z'):
            postfixexpr.append(item)
        # if left parenthesis, add to stack
        elif item == '(':
            # postfixexpr.append(item) -> this was the problem
            stack.push(item)
        # if item is )
        elif item == ')':
            # pop stack until (
            top = stack.pop()
            while top != '(':
                postfixexpr.append(top)
                top = stack.pop()
        else:
            # while not empty stack and items on stack >= precedence of item
            while not stack.isEmpty() and prec[stack.peek()] >= prec[item]:
                postfixexpr.append(stack.pop())  
            # put the item on the stack                
            stack.push(item)

    # remove all remaining operators from the stack and append to list    
    while not stack.isEmpty() :
        postfixexpr.append(stack.pop())  
       
   
    # array to string
    return " ".join(postfixexpr)