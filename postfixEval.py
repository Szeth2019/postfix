from mystack import Stack, doMath

def postfixEval(expr):

    # create stack
    stack = Stack()
   
    # conver expr to array
    exprArray = expr.split()
   
    # for each item in tokenArray
    for item in exprArray:
        # if item is a number, add to list
        if item in "123456789":
            stack.push(item)
       
        # if item is an operand
        if item in "*/+-":
            operator = item
            # pop 2 items
            # convert to int
            op2 = stack.pop()
            op1 = stack.pop()
           
            # do math with op1, op2, and operator and push back answer onto stack
            stack.push(doMath(op1, op2, operator))
   
    # return the remaining number on stack as solution    
    return stack.pop()